import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from pylab import *
from scipy import integrate
from scipy import signal
import math

#Read csv file 

columns=['field.header.seq','field.header.stamp','field.orientation.x','field.orientation.y',
         'field.orientation.z','field.orientation.w','field.angular_velocity.x','field.angular_velocity.y',
         'field.angular_velocity.z','field.linear_acceleration.x','field.linear_acceleration.y','field.linear_acceleration.z']
df=pd.read_csv("Data4_IMU.csv", usecols=columns)

columns1=['field.magnetic_field.x','field.magnetic_field.y','field.magnetic_field.z']
dg=pd.read_csv("Data4_Mag.csv", usecols=columns1)


columns=['field.header.seq','field.header.stamp','field.Altitude','field.UTM_Easting','field.UTM_Northing','field.UTM_Zone','field.UTM_Letter']
dh = pd.read_csv("Data4_GPS.csv", usecols=columns)

#get time in seconds
stamp = df['field.header.seq']
time = [((d-37932)/40) for d in stamp]

stamp1 = dh['field.header.seq']
time_gps = [(d-956) for d in stamp1]
########Eular angles
x=df['field.orientation.x']
y=df['field.orientation.y']
z=df['field.orientation.z']
w=df['field.orientation.w']
magx_raw=dg['field.magnetic_field.x']*10000
magy_raw=dg['field.magnetic_field.y']*10000
gyrox=df['field.angular_velocity.x']  
gyroy=df['field.angular_velocity.y']
gyroz=df['field.angular_velocity.z']
utm_easting = dh['field.UTM_Easting']
utm_northing =dh['field.UTM_Northing']
accx=df['field.linear_acceleration.x']
accy=df['field.linear_acceleration.y']


ysqr = y * y
t0 = +2.0 * (w * x + y * z)
t1 = +1.0 - 2.0 * (x * x + ysqr)
X = np.degrees(np.arctan2(t0, t1))

t2 = +2.0 * (w * y - z * x)
t2 = np.where(t2>+1.0,+1.0,t2)
#t2 = +1.0 if t2 > +1.0 else t2

t2 = np.where(t2<-1.0, -1.0, t2)
#t2 = -1.0 if t2 < -1.0 else t2
Y = np.degrees(np.arcsin(t2))

t3 = +2.0 * (w * z + x * y)
t4 = +1.0 - 2.0 * (ysqr + z * z)
Z = (np.arctan2(t3, t4))

roll= X
pitch=Y
yaw_imu =Z
#Calibration start and stop point for reading of magnetometer
magstart = 0
magcstop= 6600
end =37435

Xmax = max(magx_raw[magstart:magcstop])
Xmin = min(magx_raw[magstart:magcstop])

Ymax = max(magy_raw[magstart:magcstop])
Ymin = min(magy_raw[magstart:magcstop])

avg_delta_x = (Xmax - Xmin) / 2
avg_delta_y = (Ymax - Ymin) / 2
avg_delta = (avg_delta_x + avg_delta_y ) / 2

Xsf =avg_delta / avg_delta_x
Ysf = avg_delta / avg_delta_y

Xoff = (Xmax + Xmin)/2 
Yoff = (Ymax + Ymin)/2


if Xsf < 1:
    Xsf = 1
if Ysf < 1:
    Ysf = 1

print("Xsf = ",Xsf,"Ysf = ",Ysf,"\r")
print("Xoff = ",Xoff,"Yoff = ",Yoff,"\r\r")
magx_corrected=[]
magy_corrected=[]
for i in range(0, 37435):
    A= (magx_raw[i]- Xoff)*Xsf
    B= (magy_raw[i] - Yoff)*Ysf
    magx_corrected.append(A)
    magy_corrected.append(B)
    
yaw_m = np.arctan2(np.negative(magy_corrected), magx_corrected)
yaw_m_rad = np.unwrap(yaw_m[magcstop:end])

yaw_gyro = integrate.cumtrapz(gyroz[magcstop:end], time[magcstop:end]) #initial=0


# low-pass filter
sampling = 40.0
nyquist = sampling / 2
cutoff = 0.1
normal_cutoff =  cutoff / nyquist
b, a = signal.butter(1, normal_cutoff, 'low')
low_filt_yaw_mag = signal.filtfilt(b, a, yaw_m_rad)


# high-pass filter
sampling = 40.0
nyquist = sampling / 2
cutoff = 1
normal_cutoff =  cutoff / nyquist
b, a = signal.butter(1, normal_cutoff, 'high')
high_filt_yaw_gyro = signal.filtfilt(b, a, yaw_gyro)


# complementary filter
yaw_complementary_result = np.array(low_filt_yaw_mag[:-1])*.2+ np.array(yaw_gyro)*.8 # relative strengths (.2 and .8)

yaw_imu=np.unwrap(yaw_imu[magcstop:end])

 

######################8##################################
fig, may= plt.subplots()

may.scatter(magx_raw[0:165*40],magy_raw[0:165*40],s=4,c='blue')
plt.gca().set_aspect("equal")
#may.ticklabel_format(useOffset=False)
plt.legend()
plt.tight_layout()
plt.xlabel('Magnetometer measurement X (gauss)')
plt.ylabel('Magnetometer measurement y (gauss)')
plt.title('magnetormeter before calibration')

fig, may_c= plt.subplots()
may_c.scatter(magx_corrected[0:165*40],magy_corrected[0:165*40],s=4,c='blue')
plt.gca().set_aspect("equal")
#may.ticklabel_format(useOffset=False)
plt.legend()
plt.tight_layout()
plt.xlabel('Magnetometer measurement X (gauss)')
plt.ylabel('Magnetometer measurement y (gauss)')
plt.title('magnetormeter after calibration')


fig, myaw= plt.subplots()
myaw.plot(time[magcstop:end],yaw_m_rad, label='Magnetometer') 
myaw.plot(time[magcstop:end-1],yaw_gyro, label='Integrated Yaw Rate')
#may.ticklabel_format(useOffset=False)
plt.legend()
plt.tight_layout()
plt.xlabel('Time(s)')
plt.ylabel('Yaw angle in radians')
plt.title('Magnetometer vs. Yaw Integrated from Gyro')

fig, comp1= plt.subplots()
comp1.plot(time[magcstop:end],yaw_imu, label='IMU yaw')
comp1.plot(time[magcstop:end-1],yaw_complementary_result, label='Complementary Filter yaw') 
#may.ticklabel_format(useOffset=False)
plt.legend()
plt.tight_layout()
plt.xlabel('Time(s)')
plt.ylabel('Yaw angle in radians')
plt.title('IMU yaw  vs. complimentory filter result')
#show graphs



############velocity analysis##################################################################

####################part 2 :#####################################################################

relevant_acc_x = np.array(accx[magcstop:])
print(relevant_acc_x)
accx_bias_r = relevant_acc_x-relevant_acc_x[0]
plt.figure()
plt.plot(time[magcstop:], accx_bias_r , label="Accelearation plot for bias removal")
plt.legend()
plt.xlabel('Time(s)')
plt.ylabel('x-acceleration in m/s2')
plt.title('Accelearation plot for initial bias removal')

fw_vel_accx = integrate.cumtrapz(accx_bias_r, time[magcstop:end])

# differentiated gps for velocity
gps_start= int(magcstop/40)
time_gps =np.array(time_gps[gps_start:])
utm_easting = np.array(utm_easting[gps_start:])
utm_northing =np.array(utm_northing[gps_start:])
gps_vel=[]
for i in range(len(utm_easting) -1):
    x = abs(utm_easting[i] - utm_easting[i+1])
    y = abs(utm_northing[i] - utm_northing[i+1])
    t = abs(time_gps[i] - time_gps[i+1])
    gps_velocity = math.sqrt(x**2 + y**2) / t 
    gps_vel.append(gps_velocity)


accx_bias_all=[]
####for removing middle accerlation bias 
for i in range (len(accx_bias_r)) :
 
        if (abs(accx_bias_r[i]) < 0.3) :
            accx_bias_all.append((accx_bias_r[i])*0)
        else :
            accx_bias_all.append((accx_bias_r[i]))

end

plt.figure()
plt.plot(time[magcstop:], accx_bias_all, label="Integrated Forward Acceleration (Estimated)")
plt.legend()
plt.xlabel('Time(s)')
plt.ylabel('x-accelearation in m/s2')
plt.title('Integrated Forward Acceleration (Estimated)')
print(len(accx_bias_all))
fw_vel_accx_true = integrate.cumtrapz(accx_bias_all, time[magcstop:end])

sampling = 40.0
nyquist = sampling / 2
#cutoff = 0.008
cutoff = 0.0003
normal_cutoff =  cutoff / nyquist
b, a = signal.butter(2, normal_cutoff, 'high')
high_filt_vel = signal.filtfilt(b, a, (fw_vel_accx_true))


plt.figure()
plt.plot(time[magcstop:-1], fw_vel_accx, label="Integrated Forward Acceleration (Estimated)")
plt.plot(time_gps[:-1], gps_vel, label="gps estimated forward velocity(Ground truth)")
plt.plot(time[magcstop:-1], high_filt_vel, label="Integrated Forward Acceleration (bias_removed)")
plt.title('Raw Calculated Velocity vs GPS Velocity (m/s)')
plt.xlabel('Time (s)')
plt.ylabel('Velocity (m/s)')
plt.legend()

adju_veloc2 = integrate.cumtrapz(accx_bias_all- np.mean(accx_bias_all) ,time[magcstop:end])
####################part 3 :#####################################################################
##########Dead Reckoning with IMU################################################################

omega = gyroz[magcstop:-1]

computed_y_acc = high_filt_vel* omega
relevant_accy = np.array(accy[magcstop:])
y_acc = relevant_accy - relevant_accy[0]

sampling = 40.0
nyquist = sampling / 2
cutoff = 0.5
critical =  cutoff / nyquist
b, a = signal.butter(1, critical, 'low')
low_filt_y_acc = signal.filtfilt(b, a, y_acc)

plt.figure()
plt.plot(time[magcstop:-1],computed_y_acc, label='Computed y-acceleration')
plt.xlabel("Sample point number")
plt.ylabel('Acceleration (m/s^2)')
plt.title("Comparing wX' and y acceleration")
plt.plot(time[magcstop:],low_filt_y_acc, label='Observed y-acceleration')
plt.legend()

# IMU displacement  

y_obs = np.multiply(adju_veloc2 , np.cos((yaw_complementary_result)))
x_obs = np.multiply(adju_veloc2 , np.sin((yaw_complementary_result)))


print(len(x_obs))
print(len(time[magcstop:]))
x_dist=integrate.cumtrapz((x_obs),time[magcstop:-1])
y_dist= integrate.cumtrapz((y_obs),time[magcstop:-1])


utm_easting= utm_easting - utm_easting[0]
utm_northing=utm_northing - utm_northing[0]
x_dist = x_dist -x_dist[0]
y_dist= y_dist - y_dist[0]

###scaling factor of 1.5 is used to lower the graph
x_scaled= x_dist /1.5
y_scaled= y_dist /1.5
theta = -0.3
c, s = np.cos(theta), np.sin(theta)
r = np.array(((c, s), (-s, c)))
rotated_x, rotated_y = r.dot([x_scaled, y_scaled])

plt.figure()
plt.plot(utm_easting,utm_northing, label="GPS Displacement")
plt.plot(rotated_x, rotated_y, label="IMU Displacement")
plt.title("GPS Trajectory vs IMU Trajectory")
plt.xlabel("Displacement from origin in x (m)")
plt.ylabel("Displacement from origin in y (m)")
plt.plot()
plt.legend()

plt.show()
