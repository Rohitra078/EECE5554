#!/usr/bin/env python

import rospy
import serial 
import math
from std_msgs.msg import Float64,String
from sensor_msgs.msg import Imu,MagneticField
from tf.transformations import quaternion_from_euler

if __name__ == '__main__':
    SENSOR_NAME = "VN_100_IMU"
    rospy.init_node('imu_data')   #node gps_data added
    
    #adding the serial for interface with gps sesnor with the port
    serial_port = rospy.get_param('~port','/dev/ttyUSB0')
    #sampling_rate = rospy.get_param('~sampling_rate',40)
    #to check the serial port uncomment the below command
    #print(serial_port)
    
    
    serial_baud = rospy.get_param('~baudrate',115200)

    port = serial.Serial(serial_port, serial_baud, timeout=3)

    rospy.logdebug("Using gps sensor on earth "+serial_port+" at "+str(serial_baud))
    
    #publishing all the nodes sperately and also together at end as msg so can take whichever specific value or whole bunch
   
    imu_data_pub =rospy.Publisher(SENSOR_NAME+'/imu_data',Imu, queue_size =5)
    MAG_data_pub =rospy.Publisher(SENSOR_NAME+'/Magnetometer_data',MagneticField, queue_size =5)
    rospy.logdebug("Initialization complete")
    rospy.loginfo("Publishing IMU DATA")
    

    imu_msg = Imu()
    imu_msg.header.frame_id = SENSOR_NAME
    imu_msg.header.seq= 0
   

    MAG_msg = MagneticField()
    MAG_msg.header.frame_id = SENSOR_NAME
    MAG_msg.header.seq= 0
   

    try:

     while not rospy.is_shutdown():
         line= port.readline()
         #print(line)   #reading imu data here
         
         if line == '':
            rospy.logwarn("IMU: No data")
        

         else:
           #filtering for specific codewords to narrow down gps coordinates
          if line.startswith(b'$VNYMR'):

            values = line.split(b',')
            
            #orientation
            yaw = math.radians(float(values[1].decode('utf-8')))
            pitch = math.radians(float(values[2].decode('utf-8')))
            roll = math.radians(float(values[3].decode('utf-8')))
          
            #Magnetometer measurement
            Magx= float(values[4].decode('utf-8'))/10000
            Magy= float(values[5].decode('utf-8'))/10000
            Magz= float(values[6].decode('utf-8'))/10000
            
            #accelerometer mesurement
            accelx= float(values[7].decode('utf-8'))
            accely= float(values[8].decode('utf-8'))
            accelz= float(values[9].decode('utf-8'))
            
            #Gyrometer mesurement
            gyrox= float(values[10].decode('utf-8'))
            gyroy= float(values[11].decode('utf-8'))
            gyroz_raw= values[12].decode('utf-8')

            gyroz =float(gyroz_raw[:10])
            

            imu_msg.header.stamp=rospy.Time.now()
            MAG_msg.header.stamp=rospy.Time.now()  
            
            #orientation fill in IMU msg
            q = quaternion_from_euler(roll, pitch, yaw)

            pose = imu_msg.orientation
            pose.x = q[0]
            pose.y = q[1]
            pose.z = q[2]
            pose.w = q[3]
            imu_msg.orientation_covariance = [0,0,0,0,0,0,0,0,0]

            #fill gyrometer(angular velocity) value in IMU msg
            imu_msg.angular_velocity.x =gyrox
            imu_msg.angular_velocity.y =gyroy
            imu_msg.angular_velocity.z =gyroz
            imu_msg.angular_velocity_covariance = [0,0,0,0,0,0,0,0,0]

            #fill acceleration value in IMU msg
            imu_msg.linear_acceleration.x =accelx
            imu_msg.linear_acceleration.y =accely
            imu_msg.linear_acceleration.z =accelz
            imu_msg.linear_acceleration_covariance = [0,0,0,0,0,0,0,0,0]
            

            #fill Magnetic Field vector msg
            MAG_msg.magnetic_field.x=Magx
            MAG_msg.magnetic_field.y=Magy
            MAG_msg.magnetic_field.z=Magz
            MAG_msg.magnetic_field_covariance = [0,0,0,0,0,0,0,0,0]

            imu_msg.header.seq+=1   
            MAG_msg.header.seq+=1 

            #publish all data from imu_msg and mag_msg
            imu_data_pub.publish(imu_msg)
            MAG_data_pub.publish(MAG_msg)

            #to check data output uncomment the below line
            print(imu_msg)
            print(MAG_msg)
         

    except rospy.ROSInterruptException:
        port.close()
        quit() 
          
    except serial.serialutil.SerialException:
          rospy.loginfo("Shutting down gps_driver node...")
          
        
