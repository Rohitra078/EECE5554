
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from pylab import *
import utm
from mpl_toolkits import mplot3d
import statistics
from scipy.stats import norm


#Read csv file 

columns=['field.header.seq','field.header.stamp','field.orientation.x','field.orientation.y',
         'field.orientation.z','field.orientation.w','field.angular_velocity.x','field.angular_velocity.y',
         'field.angular_velocity.z','field.linear_acceleration.x','field.linear_acceleration.y','field.linear_acceleration.z']
df=pd.read_csv("imu_data.csv", usecols=columns)

columns1=['field.magnetic_field.x','field.magnetic_field.y','field.magnetic_field.z']
dg=pd.read_csv("Magnetometer_data.csv", usecols=columns1)
#get time in seconds
stamp = df['field.header.seq']
time = [((d-1)/40) for d in stamp]

########Eular angles
x=df['field.orientation.x']
y=df['field.orientation.y']
z=df['field.orientation.z']
w=df['field.orientation.w']

ysqr = y * y
t0 = +2.0 * (w * x + y * z)
t1 = +1.0 - 2.0 * (x * x + ysqr)
X = np.degrees(np.arctan2(t0, t1))

t2 = +2.0 * (w * y - z * x)
t2 = np.where(t2>+1.0,+1.0,t2)
#t2 = +1.0 if t2 > +1.0 else t2

t2 = np.where(t2<-1.0, -1.0, t2)
#t2 = -1.0 if t2 < -1.0 else t2
Y = np.degrees(np.arcsin(t2))

t3 = +2.0 * (w * z + x * y)
t4 = +1.0 - 2.0 * (ysqr + z * z)
Z = np.degrees(np.arctan2(t3, t4))

roll= X
pitch=Y
yaw =Z

fig, rx= plt.subplots()

rx.scatter(time,roll,s=4,c='blue')
#ax.ticklabel_format(useOffset=False)
plt.tight_layout()
plt.xlabel('Time(s)')
plt.ylabel('Roll angle in deg')
plt.title('Time series plot')

fig, px= plt.subplots()

px.scatter(time,pitch,s=4,c='blue')
#ax.ticklabel_format(useOffset=False)
plt.tight_layout()
plt.xlabel('Time(s)')
plt.ylabel('Pitch angle in deg')
plt.title('Time series plot')

fig, yx= plt.subplots()

yx.scatter(time,yaw,s=4,c='blue')
#ax.ticklabel_format(useOffset=False)
plt.tight_layout()
plt.xlabel('Time(s)')
plt.ylabel('Yaw angle in deg')
plt.title('Time series plot')

#get all mean and std deaviation for gyroscore or angular velocity 
mean_ang_x= statistics.mean(df['field.angular_velocity.x'])
std_ang_x= np.std(df['field.angular_velocity.x'])

mean_ang_y= statistics.mean(df['field.angular_velocity.y'])
std_ang_y= np.std(df['field.angular_velocity.y'])

mean_ang_z= statistics.mean(df['field.angular_velocity.z'])
std_ang_z= np.std(df['field.angular_velocity.z'])

print(f"{mean_ang_x=} {std_ang_x=} \n {mean_ang_y=} {std_ang_y=} \n {mean_ang_z=} {std_ang_z=}")
######################1##################################
#plot 1st graph for angular velocity 
fig, ax= plt.subplots()

ax.scatter(time,df['field.angular_velocity.x'],s=4,c='blue')
#ax.ticklabel_format(useOffset=False)
plt.hlines(y=mean_ang_x,xmin=0 ,xmax=865 ,colors='aqua', linestyles='-', lw=2, label='Mean')
plt.legend()
plt.tight_layout()
plt.xlabel('Time(s)')
plt.ylabel('angular velocity x (rad/s)')
plt.title('Time series plot')

#add value for zone and letter in plot
plt.figtext(0.2, 0.9,f"Mean  = {str(mean_ang_x)}")
plt.figtext(0.2, 0.87, f"std deaviation = {str(std_ang_x)[0:10]}")

ang_x=sort(df['field.angular_velocity.x'])
fig, bx= plt.subplots()
bx.plot(ang_x, norm.pdf(ang_x, mean_ang_x, std_ang_x),label=f'μ:{str(mean_ang_x)}, σ: {str(std_ang_x)[0:10]}')
plt.xlabel('field angular velocity x (rad/s)')
plt.ylabel('Normal Distribution')
plt.legend()

####################2########################
#plot 1st graph for angular velocity 
fig, ay= plt.subplots()

ay.scatter(time,df['field.angular_velocity.y'],s=4,c='blue')
#ay.ticklabel_format(useOffset=False)
plt.hlines(y=mean_ang_y,xmin=0 ,xmax=865 ,colors='aqua', linestyles='-', lw=2, label='Mean')
plt.legend()
plt.tight_layout()
plt.xlabel('Time(s)')
plt.ylabel('angular velocity y (rad/s)')
plt.title('Time series plot')

#add value for zone and letter in plot
plt.figtext(0.2, 0.9,f"Mean  = {str(mean_ang_y)}")
plt.figtext(0.2, 0.87, f"std deaviation = {str(std_ang_y)[0:10]}")

ang_y=sort(df['field.angular_velocity.y'])
fig, by= plt.subplots()
by.plot(ang_y, norm.pdf(ang_y, mean_ang_y, std_ang_y),label=f'μ:{str(mean_ang_y)}, σ: {str(std_ang_y)[0:10]}')
plt.xlabel('field angular velocity y (rad/s)')
plt.ylabel('Normal Distribution')
plt.legend()

######################3##################################
#plot 1st graph for angular velocity 
fig, az= plt.subplots()

az.scatter(time,df['field.angular_velocity.z'],s=4,c='blue')
#az.ticklabel_format(useOffset=False)
plt.hlines(y=mean_ang_z,xmin=0 ,xmax=865 ,colors='aqua', linestyles='-', lw=2, label='Mean')
plt.legend()
plt.tight_layout()
plt.xlabel('Time(s)')
plt.ylabel('angular velocity z(rad/s)')
plt.title('Time series plot')

#add value for zone and letter in plot
plt.figtext(0.2, 0.9,f"Mean  = {str(mean_ang_z)}")
plt.figtext(0.2, 0.87, f"std deaviation = {str(std_ang_z)[0:10]}")

ang_z=sort(df['field.angular_velocity.z'])
fig, bz= plt.subplots()
bz.plot(ang_z, norm.pdf(ang_z, mean_ang_z, std_ang_z),label=f'μ:{str(mean_ang_z)}, σ: {str(std_ang_z)[0:10]}')
plt.xlabel('field angular velocity z (rad/s)')
plt.ylabel('Normal Distribution')
plt.legend()

###########
###########
#get all mean and std deaviation for linear acceleration m/s2
mean_acc_x= statistics.mean(df['field.linear_acceleration.x'])
std_acc_x= np.std(df['field.linear_acceleration.x'])

mean_acc_y= statistics.mean(df['field.linear_acceleration.y'])
std_acc_y= np.std(df['field.linear_acceleration.y'])

mean_acc_z= statistics.mean(df['field.linear_acceleration.z'])
std_acc_z= np.std(df['field.linear_acceleration.z'])

print(f"{mean_acc_x=} {std_acc_x=} \n {mean_acc_y=} {std_acc_y=} \n {mean_acc_z=} {std_acc_z=}")

######################4##################################
#plot 1st graph for linear acceleration
fig, acx= plt.subplots()

acx.scatter(time,df['field.linear_acceleration.x'],s=4,c='blue')
#acx.ticklabel_format(useOffset=False)
plt.hlines(y=mean_acc_x,xmin=0 ,xmax=865 ,colors='aqua', linestyles='-', lw=2, label='Mean')
plt.legend()
plt.tight_layout()
plt.xlabel('Time(s)')
plt.ylabel('linear acceleration x (m/s2)')
plt.title('Time series plot')

#add value for zone and letter in plot
plt.figtext(0.2, 0.9,f"Mean  = {str(mean_acc_x)}")
plt.figtext(0.2, 0.87, f"std deaviation = {str(std_acc_x)[0:10]}")

acc_x=sort(df['field.linear_acceleration.x'])
fig, bcx= plt.subplots()
bcx.plot(acc_x, norm.pdf(acc_x, mean_acc_x, std_acc_x),label=f'μ:{str(mean_acc_x)}, σ: {str(std_acc_x)[0:10]}')
plt.xlabel('linear acceleration x (m/s2)')
plt.ylabel('Normal Distribution')
plt.legend()

######################5##################################
#plot 1st graph for linear acceleration
fig, acy= plt.subplots()

acy.scatter(time,df['field.linear_acceleration.y'],s=4,c='blue')
#acy.ticklabel_format(useOffset=False)
plt.hlines(y=mean_acc_y,xmin=0 ,xmax=865 ,colors='aqua', linestyles='-', lw=2, label='Mean')
plt.legend()
plt.tight_layout()
plt.xlabel('Time(s)')
plt.ylabel('linear acceleration y (m/s2)')
plt.title('Time series plot')

#add value for zone and letter in plot
plt.figtext(0.2, 0.9,f"Mean  = {str(mean_acc_y)}")
plt.figtext(0.2, 0.87, f"std deaviation = {str(std_acc_y)[0:10]}")

acc_y=sort(df['field.linear_acceleration.y'])
fig, bcy= plt.subplots()
bcy.plot(acc_y, norm.pdf(acc_y, mean_acc_y, std_acc_y),label=f'μ:{str(mean_acc_y)}, σ: {str(std_acc_y)[0:10]}')
plt.xlabel('linear acceleration y (m/s2)')
plt.ylabel('Normal Distribution')
plt.legend()

######################6##################################
#plot 1st graph for linear acceleration
fig, acz= plt.subplots()

acz.scatter(time,df['field.linear_acceleration.z'],s=4,c='blue')
#acz.ticklabel_format(useOffset=False)
plt.hlines(y=mean_acc_z,xmin=0 ,xmax=865 ,colors='aqua', linestyles='-', lw=2, label='Mean')
plt.legend()
plt.tight_layout()
plt.xlabel('Time(s)')
plt.ylabel('linear acceleration z (m/s2)')
plt.title('Time series plot')

#add value for zone and letter in plot
plt.figtext(0.2, 0.9,f"Mean  = {str(mean_acc_z)}")
plt.figtext(0.2, 0.87, f"std deaviation = {str(std_acc_z)[0:10]}")

acc_z=sort(df['field.linear_acceleration.z'])
fig, bcz= plt.subplots()
bcz.plot(acc_z, norm.pdf(acc_z, mean_acc_z, std_acc_z),label=f'μ:{str(mean_acc_z)}, σ: {str(std_acc_z)[0:10]}')
plt.xlabel('linear acceleration z (m/s2)')
plt.ylabel('Normal Distribution')
plt.legend()

############
############
#get all mean and std deaviation for Magnetometer measurement tesla coverted to gauss
mean_mag_x= statistics.mean(dg['field.magnetic_field.x']*10000)
std_mag_x= np.std(dg['field.magnetic_field.x']*10000)

mean_mag_y= statistics.mean(dg['field.magnetic_field.y']*10000)
std_mag_y= np.std(dg['field.magnetic_field.y']*10000)

mean_mag_z= statistics.mean(dg['field.magnetic_field.z']*10000)
std_mag_z= np.std(dg['field.magnetic_field.z']*10000)

print(f"{mean_mag_x=} {std_mag_x=} \n {mean_mag_y=} {std_mag_y=} \n {mean_mag_z=} {std_mag_z=}")
######################7##################################
#plot 1st graph for Magnetometer measurement tesla
fig, max= plt.subplots()

max.scatter(time,dg['field.magnetic_field.x']*10000,s=4,c='blue')
#max.ticklabel_format(useOffset=False)
plt.hlines(y=mean_mag_x,xmin=0 ,xmax=865 ,colors='aqua', linestyles='-', lw=2, label='Mean')
plt.legend()
plt.tight_layout()
plt.xlabel('Time(s)')
plt.ylabel('Magnetometer measurement x (Gauss)')
plt.title('Time series plot')

#add value for zone and letter in plot
plt.figtext(0.2, 0.9,f"Mean  = {str(mean_mag_x)}")
plt.figtext(0.2, 0.87, f"std deaviation = {str(std_mag_x)}")

mag_x=sort(dg['field.magnetic_field.x']*10000)
fig, mbx= plt.subplots()
mbx.plot(mag_x, norm.pdf(mag_x, mean_mag_x, std_mag_x),label=f'μ:{str(mean_mag_x)}, σ: {str(std_mag_x)}')
plt.xlabel('Magnetometer measurement x (gauss)')
plt.ylabel('Normal Distribution')
plt.legend()

######################8##################################
#plot 1st graph for Magnetometer measurement tesla
fig, may= plt.subplots()

may.scatter(time,dg['field.magnetic_field.y']*10000,s=4,c='blue')
#may.ticklabel_format(useOffset=False)
plt.hlines(y=mean_mag_y,xmin=0 ,xmax=865 ,colors='aqua', linestyles='-', lw=2, label='Mean')
plt.legend()
plt.tight_layout()
plt.xlabel('Time(s)')
plt.ylabel('Magnetometer measurement y (gauss)')
plt.title('Time series plot')

#add value for zone and letter in plot
plt.figtext(0.2, 0.9,f"Mean  = {str(mean_mag_y)}")
plt.figtext(0.2, 0.87, f"std deaviation = {str(std_mag_y)[0:10]}")

mag_y=sort(dg['field.magnetic_field.y']*10000)
fig, mby= plt.subplots()
mby.plot(mag_y, norm.pdf(mag_y, mean_mag_y, std_mag_y),label=f'μ:{str(mean_mag_y)}, σ: {str(std_mag_y)}')
plt.xlabel('Magnetometer measurement y (gauss)')
plt.ylabel('Normal Distribution')
plt.legend()

######################9##################################
#plot 1st graph for Magnetometer measurement tesla
fig, maz= plt.subplots()

maz.scatter(time,dg['field.magnetic_field.z']*10000,s=4,c='blue')
#max.ticklabel_format(useOffset=False)
plt.hlines(y=mean_mag_z,xmin=0 ,xmax=865 ,colors='aqua', linestyles='-', lw=2, label='Mean')
plt.legend()
plt.tight_layout()
plt.xlabel('Time(s)')
plt.ylabel('Magnetometer measurement z (gauss)')
plt.title('Time series plot')

#add value for zone and letter in plot
plt.figtext(0.2, 0.9,f"Mean  = {str(mean_mag_z)}")
plt.figtext(0.2, 0.87, f"std deaviation = {str(std_mag_z)}")

mag_z=sort(dg['field.magnetic_field.z']*10000)
fig, mbz= plt.subplots()
mbz.plot(mag_z, norm.pdf(mag_z, mean_mag_z, std_mag_z),label=f'μ:{str(mean_mag_z)}, σ: {str(std_mag_z)}')
plt.xlabel('Magnetometer measurement z (gauss)')
plt.ylabel('Normal Distribution')
plt.legend()

######
######



#show graphs
plt.show()
