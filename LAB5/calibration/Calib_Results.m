% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 3125.050008829369290 ; 3099.828882372393764 ];

%-- Principal point:
cc = [ 1569.452340895885754 ; 2060.833731808186258 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.053258548656381 ; -0.131031321329459 ; -0.003764123383314 ; 0.001271795115283 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 6.851630256598078 ; 7.146056238387144 ];

%-- Principal point uncertainty:
cc_error = [ 4.419263966545640 ; 5.115950494542286 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.005085405360107 ; 0.013311646064324 ; 0.000555551096178 ; 0.000516315383393 ; 0.000000000000000 ];

%-- Image size:
nx = 3120;
ny = 4160;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 20;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ 3.105778e+00 ; -8.630553e-04 ; 4.369540e-02 ];
Tc_1  = [ -1.063722e+02 ; 1.248082e+02 ; 2.763419e+02 ];
omc_error_1 = [ 2.040098e-03 ; 6.129225e-04 ; 3.271641e-03 ];
Tc_error_1  = [ 4.115088e-01 ; 4.744956e-01 ; 6.770746e-01 ];

%-- Image #2:
omc_2 = [ 2.830232e+00 ; -9.035252e-02 ; 1.212483e-01 ];
Tc_2  = [ -9.479784e+01 ; 1.157373e+02 ; 2.697343e+02 ];
omc_error_2 = [ 2.148068e-03 ; 7.342961e-04 ; 3.078409e-03 ];
Tc_error_2  = [ 4.085183e-01 ; 4.794485e-01 ; 6.788278e-01 ];

%-- Image #3:
omc_3 = [ 3.055020e+00 ; 1.963233e-02 ; -4.725936e-01 ];
Tc_3  = [ -1.295177e+02 ; 1.303388e+02 ; 3.601769e+02 ];
omc_error_3 = [ 2.209548e-03 ; 8.594324e-04 ; 3.297628e-03 ];
Tc_error_3  = [ 5.170579e-01 ; 6.014799e-01 ; 7.844907e-01 ];

%-- Image #4:
omc_4 = [ 2.743584e+00 ; 5.250970e-03 ; 2.634059e-02 ];
Tc_4  = [ -1.053695e+02 ; 8.731089e+01 ; 2.452801e+02 ];
omc_error_4 = [ 2.010103e-03 ; 7.403431e-04 ; 2.658036e-03 ];
Tc_error_4  = [ 3.644062e-01 ; 4.319321e-01 ; 5.979057e-01 ];

%-- Image #5:
omc_5 = [ -2.776004e+00 ; -3.627357e-02 ; -2.704016e-02 ];
Tc_5  = [ -1.100962e+02 ; 1.531603e+02 ; 3.359475e+02 ];
omc_error_5 = [ 1.876048e-03 ; 7.047502e-04 ; 2.739552e-03 ];
Tc_error_5  = [ 4.895515e-01 ; 5.416595e-01 ; 6.760588e-01 ];

%-- Image #6:
omc_6 = [ -2.735523e+00 ; -3.882851e-01 ; -4.446585e-01 ];
Tc_6  = [ -1.363887e+02 ; 1.291538e+02 ; 3.075036e+02 ];
omc_error_6 = [ 1.902216e-03 ; 8.961825e-04 ; 2.623028e-03 ];
Tc_error_6  = [ 4.548891e-01 ; 5.109744e-01 ; 6.790640e-01 ];

%-- Image #7:
omc_7 = [ 3.029317e+00 ; 1.120751e-01 ; 5.719811e-01 ];
Tc_7  = [ -7.563925e+01 ; 1.406223e+02 ; 2.889702e+02 ];
omc_error_7 = [ 2.204751e-03 ; 8.695208e-04 ; 3.183778e-03 ];
Tc_error_7  = [ 4.430762e-01 ; 4.966551e-01 ; 7.567773e-01 ];

%-- Image #8:
omc_8 = [ 3.135663e+00 ; 4.087277e-03 ; 2.053442e-03 ];
Tc_8  = [ -1.122705e+02 ; 1.396727e+02 ; 2.940147e+02 ];
omc_error_8 = [ 2.102096e-03 ; 6.395160e-04 ; 3.472540e-03 ];
Tc_error_8  = [ 4.375149e-01 ; 5.010215e-01 ; 7.187770e-01 ];

%-- Image #9:
omc_9 = [ 2.626732e+00 ; -3.209794e-01 ; 1.755880e-01 ];
Tc_9  = [ -7.716907e+01 ; 1.192182e+02 ; 2.285710e+02 ];
omc_error_9 = [ 2.023850e-03 ; 8.107137e-04 ; 2.551357e-03 ];
Tc_error_9  = [ 3.519533e-01 ; 4.180489e-01 ; 5.883959e-01 ];

%-- Image #10:
omc_10 = [ 2.505258e+00 ; -4.873041e-02 ; 6.428655e-02 ];
Tc_10  = [ -9.831566e+01 ; 2.965436e+01 ; 2.286485e+02 ];
omc_error_10 = [ 1.977624e-03 ; 9.546504e-04 ; 2.214352e-03 ];
Tc_error_10  = [ 3.294521e-01 ; 3.952259e-01 ; 5.765231e-01 ];

%-- Image #11:
omc_11 = [ 2.477725e+00 ; 4.947356e-02 ; -1.099966e-01 ];
Tc_11  = [ -1.152578e+02 ; 3.180767e+01 ; 2.405108e+02 ];
omc_error_11 = [ 1.955136e-03 ; 9.629261e-04 ; 2.174685e-03 ];
Tc_error_11  = [ 3.436650e-01 ; 4.164214e-01 ; 5.750743e-01 ];

%-- Image #12:
omc_12 = [ 2.405897e+00 ; -3.087339e-01 ; 2.528029e-01 ];
Tc_12  = [ -7.242936e+01 ; 8.739042e+00 ; 2.106941e+02 ];
omc_error_12 = [ 1.992509e-03 ; 1.099289e-03 ; 2.068279e-03 ];
Tc_error_12  = [ 3.050676e-01 ; 3.639452e-01 ; 5.758481e-01 ];

%-- Image #13:
omc_13 = [ 2.578450e+00 ; -3.440107e-01 ; 2.172555e-01 ];
Tc_13  = [ -7.161903e+01 ; 6.693813e+01 ; 2.481412e+02 ];
omc_error_13 = [ 2.030080e-03 ; 9.613824e-04 ; 2.461799e-03 ];
Tc_error_13  = [ 3.636372e-01 ; 4.327646e-01 ; 6.487405e-01 ];

%-- Image #14:
omc_14 = [ 2.674167e+00 ; -8.400912e-01 ; 2.158797e-01 ];
Tc_14  = [ -1.517571e+01 ; 1.104928e+02 ; 2.947235e+02 ];
omc_error_14 = [ 2.175930e-03 ; 1.131780e-03 ; 3.030765e-03 ];
Tc_error_14  = [ 4.351711e-01 ; 5.150335e-01 ; 7.564566e-01 ];

%-- Image #15:
omc_15 = [ 2.836998e+00 ; 7.254968e-01 ; -1.404074e-01 ];
Tc_15  = [ -1.563084e+02 ; 3.525251e+01 ; 3.180194e+02 ];
omc_error_15 = [ 2.173007e-03 ; 9.700888e-04 ; 3.256396e-03 ];
Tc_error_15  = [ 4.602877e-01 ; 5.427523e-01 ; 7.667267e-01 ];

%-- Image #16:
omc_16 = [ 2.722044e+00 ; 1.321617e-01 ; -3.166921e-01 ];
Tc_16  = [ -1.474202e+02 ; 8.223278e+01 ; 3.191996e+02 ];
omc_error_16 = [ 2.123920e-03 ; 9.284640e-04 ; 2.753572e-03 ];
Tc_error_16  = [ 4.576679e-01 ; 5.528812e-01 ; 7.361242e-01 ];

%-- Image #17:
omc_17 = [ 2.801070e+00 ; -8.992321e-02 ; 4.136973e-01 ];
Tc_17  = [ -7.983291e+01 ; 1.065476e+02 ; 2.794626e+02 ];
omc_error_17 = [ 2.226078e-03 ; 8.708663e-04 ; 3.044067e-03 ];
Tc_error_17  = [ 4.262359e-01 ; 4.948422e-01 ; 7.438503e-01 ];

%-- Image #18:
omc_18 = [ 2.658927e+00 ; 2.935142e-01 ; -2.760058e-01 ];
Tc_18  = [ -1.382006e+02 ; 6.756812e+01 ; 3.033967e+02 ];
omc_error_18 = [ 2.075489e-03 ; 9.229022e-04 ; 2.635204e-03 ];
Tc_error_18  = [ 4.363419e-01 ; 5.239374e-01 ; 6.929950e-01 ];

%-- Image #19:
omc_19 = [ 2.649199e+00 ; 8.907603e-02 ; -8.252885e-02 ];
Tc_19  = [ -1.097559e+02 ; 7.882652e+01 ; 2.659116e+02 ];
omc_error_19 = [ 2.030428e-03 ; 8.179900e-04 ; 2.605353e-03 ];
Tc_error_19  = [ 3.895746e-01 ; 4.642766e-01 ; 6.345817e-01 ];

%-- Image #20:
omc_20 = [ 3.069668e+00 ; 3.204887e-02 ; -3.941625e-01 ];
Tc_20  = [ -1.213486e+02 ; 1.352867e+02 ; 3.169414e+02 ];
omc_error_20 = [ 2.054910e-03 ; 7.316885e-04 ; 3.091602e-03 ];
Tc_error_20  = [ 4.591616e-01 ; 5.313437e-01 ; 7.050824e-01 ];

