#!/usr/bin/env python

import rospy
import serial 
from std_msgs.msg import Float64,String
from gps_driver.msg import gps_data_lab_2_msg
import utm

if __name__ == '__main__':
    SENSOR_NAME = "gps"
    rospy.init_node('gps_data')   #node gps_data added
    
    #adding the serial for interface with gps sesnor with the port
    serial_port = rospy.get_param('~port','/dev/ttyACM0')

    #to check the serial port uncomment the below command
    #print(serial_port)
    
    
    serial_baud = rospy.get_param('~baudrate',4800)
    port = serial.Serial(serial_port, serial_baud, timeout=3.)

    rospy.logdebug("Using gps sensor on earth "+serial_port+" at "+str(serial_baud))
    
    #publishing all the nodes sperately and also together at end as msg so can take whichever specific value or whole bunch
   
    GPS_data =rospy.Publisher(SENSOR_NAME+'/GPS_data',gps_data_lab_2_msg, queue_size =5)

    rospy.logdebug("Initialization complete")
    rospy.loginfo("Publishing GPS DATA")

    gps_msg = gps_data_lab_2_msg()
    gps_msg.header.frame_id = "gps"
    gps_msg.header.seq= 0

    try:

     while not rospy.is_shutdown():
         line= port.readline()
         #print(line)   #reading gps data here

         if line == '':
            rospy.logwarn("GPS: No data")
        

         else:
           #filtering for specific codewords to narrow down gps coordinates
          if line.startswith(b"$GNGGA"):

            values = line.split(b',')
            gps_data_header=rospy.Time.now()
            deg_m_m_latitude = values[2].decode('utf-8')
            gps_msg.header.stamp=rospy.Time.now() 
            #lat_direction =values[3]
            #long_direction =values[4]
            #converting the lattitude from dd mm.mmm to ddd
            A=float(str(deg_m_m_latitude)[:2])
            B=float(str(deg_m_m_latitude)[2:])
            C=B/60
            latitude = A + C
            
            #converting the longitude from ddd mm.mmm to ddd
            deg_m_m_longitude = values[4].decode('utf-8')
            D=float(str(deg_m_m_longitude)[:3])
            E=float(str(deg_m_m_longitude)[3:])
            F=E/60
            #as we have longitude in West and are in us Hardcoding the driver ,adding the sign negative for UTM
            longitude = -(D + F)

            altitude = float(values[9].decode('utf-8'))
            fix_q=float(values[6].decode('utf-8'))
            
            if fix_q == 1 :
              print('current readings are single quality')
            elif fix_q == 4 :
              print('current readings are Rtk fix quality')
            elif fix_q ==  5:
              print('current gps readings are Rtk float quality')

            #using utm for utm_easting and utm_northing data 
            utm_data=utm.from_latlon(float(latitude), float(longitude))
            utm_easting = utm_data[0]
            utm_northing = utm_data[1]
            zone = utm_data[2]
            letter =utm_data[3]

            #adding all values to gps_msg to broadcast
            gps_msg.lat= latitude 

            gps_msg.long = longitude
            gps_msg.alt = altitude
            gps_msg.utm_e = utm_easting
            gps_msg.utm_n= utm_northing
            gps_msg.zone_a= str(zone)
            gps_msg.fix_quality = fix_q
            gps_msg.letter_a= letter
            gps_msg.header.seq+=1
            GPS_data.publish((gps_msg))

            #to check data output uncomment the below line
            print(gps_msg)
         

    except rospy.ROSInterruptException:
        port.close()
        quit() 
          
    except serial.serialutil.SerialException:
          rospy.loginfo("Shutting down gps_driver node...")
          
        
