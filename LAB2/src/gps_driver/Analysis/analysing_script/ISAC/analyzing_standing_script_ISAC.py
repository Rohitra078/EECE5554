
import matplotlib.pyplot as plt
import pandas as pd
from pylab import *
import utm
from mpl_toolkits import mplot3d

#Read csv file 
#data = pd.read_csv("Lab2_Standing_Columbus_Data.csv")
columns=['field.header.seq','field.header.stamp','field.Altitude','field.UTM_Easting','field.UTM_Northing','field.UTM_Zone','field.UTM_Letter','field.GPS_Quality_Indicator']
df=pd.read_csv("Lab2_Standing_ISEC_Data.csv", usecols=columns)
#get time in seconds
stamp = df['field.header.seq']
df["time"] = [(d-3416)/5 for d in stamp]

#get utm_easting and utm_northing and remove excessive decimals
e= df['field.UTM_Easting'] 
n= df['field.UTM_Northing'] 
x= df["field.UTM_Zone"]
y=df["field.UTM_Letter"]
z=df["field.Altitude"]
u= df["field.GPS_Quality_Indicator"]

utm_east= e
utm_north=n
Altitude=z
#taken true value from google maps : 
#lat : 42.3374351
#long :-71.0865941
#altitude :-10.225 m 
true_value= utm.from_latlon(42.3374351, -71.0865941)
#true_utm_e= statistics.mean(utm_east)
#true_utm_n= statistics.mean(utm_north)
true_utm_e= true_value[0]
true_utm_n= true_value[1]
true_alt= -10.225

#produce error
x_error = [abs(true_utm_e -f) for f in utm_east]
y_error = [abs(true_utm_n- g) for g in utm_north]

#get combined error
error_m = [(((x_error[h])**2 +(y_error[h])**2))**(1/2) for h in range (len(x_error))]

error=[k for k in error_m]

#find standard deviation 
utme=np.array(utm_east)
utmn=np.array(utm_north)
std_e= np.std(utme)
std_n=np.std(utmn)

arr = np.vstack((utme,utmn))

std_1 = np.std(arr,axis=0)

#get the bounding box coordinates
goodderr_h=[true_utm_e+std_e ,true_utm_e+std_e,true_utm_e-std_e,true_utm_e-std_e,true_utm_e+std_e]

goodderr_l=[true_utm_n+std_n,true_utm_n-std_n,true_utm_n-std_n,true_utm_n+std_n,true_utm_n+std_n]


colors= df['field.GPS_Quality_Indicator']

#plot 1st graph for utm_northing and utm_easting
fig, ax = plt.subplots()

sc=ax.scatter(utm_east,utm_north,s=5,c=colors,cmap='Accent')

#ax.ticklabel_format(useOffset=False)
plt.plot(true_utm_e,true_utm_n,'o',c= 'coral')

plt.xticks(rotation=90)
plt.tight_layout()

#lable plot
plt.xlabel('UTM_EASTING(M)')
plt.ylabel('UTM_NORTHING(M)')
plt.title('gps_sensor_standing_data_in_UTM')

#add value for zone and letter in plot
plt.figtext(.8, .7,f"Zone   = {x[1]}")
plt.figtext(.8, .75, f"Letter = {y[1]}")
l=ax.legend(*sc.legend_elements())
l.get_texts()[0].set_text('single')
l.get_texts()[1].set_text('Rtk_fix')
l.get_texts()[2].set_text('Rtk_float')

#plot second graph for error estimation
fig, bx=plt.subplots()
bc=bx.scatter(df["time"],error,s=3,c=colors,cmap='Accent')
#bx.ticklabel_format(useOffset=False)
plt.xticks(rotation=90)
plt.tight_layout()

#lable plot
plt.xlabel('Time(s)')
plt.ylabel('Standard error(m)')
plt.title('gps_sensor_error_in_standing_data_in_UTM')
q=bx.legend(*bc.legend_elements())
q.get_texts()[0].set_text('single')
q.get_texts()[1].set_text('Rtk_fix')
q.get_texts()[2].set_text('Rtk_float')



fig, cx=plt.subplots()
cx = plt.axes(projection='3d')
cc=cx.scatter3D(utm_east,utm_north,Altitude,s=4,c=colors, cmap='Accent')

cx.set_xlabel('UTM_EASTING(M)')
cx.set_ylabel('UTM_NORTHING(M)')
cx.set_zlabel('Altitude(M)')

cx.scatter(true_utm_e,true_utm_n,true_alt,'o',c= 'coral')

s=cx.legend(*cc.legend_elements())
s.get_texts()[0].set_text('single')
s.get_texts()[1].set_text('Rtk_fix')
s.get_texts()[2].set_text('Rtk_float')
plt.title('gps_sensor_3D_data_in_UTM')


#show graphs
plt.show()
