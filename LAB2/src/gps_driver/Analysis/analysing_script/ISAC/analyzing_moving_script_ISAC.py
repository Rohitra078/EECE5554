
import matplotlib.pyplot as plt
import pandas as pd
import numpy
from pylab import *
from mpl_toolkits import mplot3d

#Reading the csv file 

columns=['field.header.seq','field.header.stamp','field.Altitude','field.UTM_Easting','field.UTM_Northing','field.UTM_Zone','field.UTM_Letter','field.GPS_Quality_Indicator']
df = pd.read_csv("Lab2_Walking_ISEC_Data.csv", usecols=columns)

#putting time 

stamp = df['field.header.seq']
w=df["time"] = [(d-6907)/5 for d in stamp]

#calculate the line equation and remove excessive decimal points 
e= df['field.UTM_Easting'] #.astype(str).str[:6])
n= df['field.UTM_Northing'] #.astype(str).str[:7])
x= df["field.UTM_Zone"]
y=df["field.UTM_Letter"]
z=df["field.Altitude"]
#u= df["field.GPS_Quality_Indicator"]
colors= df['field.GPS_Quality_Indicator']

utm_east= e
utm_north= n
Altitude=z

#FOR 1ST LINE 
utme1= utm_east[1:500]
utmn1=utm_north[1:500]
m, b = np.polyfit(utme1, utmn1, 1)
time_e_1=w[1:500]
col_1= colors[1:500]
#get the equation parameters
#slope=float(c[0])
#intercept=float(c[1])

#find eastimated utm_north value
utm_north_eastimated=m*utme1+b
#print(utm_north_eastimated)

error = utm_north_eastimated - utmn1
print(len(error))
print(len(time_e_1))

#find R_square value
#corr_matrix = numpy.corrcoef(utmn1, utm_north_eastimated)
#corr = corr_matrix[0,1]
#R_sq = corr**2
#R_sq=str(round(R_sq, 2))
#print(f"R2 value for bottom line is {R_sq}")


#find error with respect to the best straight line 
#error = [(utm_north_eastimated[g]-utmn1[g])*1000 for g in range(len(utmn1))]

#For 2nd line

utme2= utm_east[666:1243]
utmn2=utm_north[666:1243]
m1, b1=np.polyfit(utme2, utmn2, 1)
time_e_2=w[666:1243]
col_2= colors[666:1243]

#find eastimated utm_north value
utm_north_eastimated_1=m1*utme2 + b1

#find R_square value
corr_matrix = numpy.corrcoef(utmn2, utm_north_eastimated_1)
corr_1 = corr_matrix[0,1]
R_sq_new = corr_1**2
R_sq_1=str(round(R_sq_new, 2))
print(f"R2 value for top line is {R_sq_1}")

#find error with respect to the best straight line 
error_2 = utm_north_eastimated_1 - utmn2

#main plot to explain fix and float
fig, zx = plt.subplots()
nx=zx.scatter(utm_east,utm_north,s=5,c=colors,cmap='tab10')
plt.xlabel('UTM_EASTING(M)')
plt.ylabel('UTM_NORTHING(M)')
plt.title('gps_sensor_walking_data_in_UTM')
plt.figtext(.2, .65,f"Zone   = {x[1]}")
plt.figtext(.2, .7, f"Letter = {y[1]}")
#plt.figtext(.8, .75,f'R2: {R_sq}')
r=zx.legend(*nx.legend_elements())
r.get_texts()[0].set_text('single')
r.get_texts()[1].set_text('Rtk_fix')
r.get_texts()[2].set_text('Rtk_float')



#Plot the 1st graph with utm_easting and utm_northing
fig, ax = plt.subplots()
sc=ax.scatter(utm_east,utm_north,s=5,c=colors,cmap='tab10')
plt.plot(utme1, m*utme1+b,c='red')
plt.plot(utme2, m1*utme2+b1,c='black')
#ax.ticklabel_format(useOffset=False)
plt.xticks(rotation=90)
plt.tight_layout()


#lable plot
plt.xlabel('UTM_EASTING(M)')
plt.ylabel('UTM_NORTHING(M)')
plt.title('gps_sensor_walking_data_in_UTM')

#Add description of letter and zone and r2
plt.figtext(.2, .65,f"Zone   = {x[1]}")
plt.figtext(.2, .7, f"Letter = {y[1]}")
#plt.figtext(.8, .75,f'R2: {R_sq}')
l=ax.legend(*sc.legend_elements())
l.get_texts()[0].set_text('single')
l.get_texts()[1].set_text('Rtk_fix')
l.get_texts()[2].set_text('Rtk_float')

#plot 2nd graph for error eastimation
fig, bx=plt.subplots()
bx.scatter(time_e_1,error,s=3,c=col_1,cmap='tab10')

bx.ticklabel_format(useOffset=False)
plt.xticks(rotation=90)
plt.tight_layout()

#lable plot
plt.xlabel('Time(s)')
plt.ylabel('standard error in m')
plt.title('gps_sensor_error_in_walking_data_in_UTM red line path')

#plot 3rd graph for error eastimation
print(len(time_e_2))
print(len(error_2))
print(len(col_2))
fig, dx=plt.subplots()
dx.scatter(time_e_2,error_2,s=3,c=col_2,cmap='tab10')

#dx.ticklabel_format(useOffset=False)
plt.xticks(rotation=90)
plt.tight_layout()
#lable plot
plt.xlabel('Time(s)')
plt.ylabel('standard error in m')
plt.title('gps_sensor_error_in_walking_data_in_UTM blue line path')


fig, cx=plt.subplots()
cx = plt.axes(projection='3d')
cc=cx.scatter3D(utm_east,utm_north,Altitude,s=4,c=colors, cmap='tab10')
cx.set_xlabel('UTM_EASTING(M)')
cx.set_ylabel('UTM_NORTHING(M)')
cx.set_zlabel('Altitude(M)')
n=cx.legend(*cc.legend_elements())
n.get_texts()[0].set_text('single')
n.get_texts()[1].set_text('Rtk_fix')
n.get_texts()[2].set_text('Rtk_float')

#show plot
plt.show()
