
import matplotlib.pyplot as plt
import pandas as pd
from pylab import *
import statistics

#Read csv file 
columns = ["header.stamp.secs","header.seq","utm_e", "utm_n","zone_a","letter_a"]
df = pd.read_csv("gps-stat_data.csv", usecols=columns)

#get time in seconds
stamp = df['header.seq']
df["time"] = [d-5 for d in stamp]

#get utm_easting and utm_northing and remove excessive decimals
e= (df['utm_e'].astype(str).str[:6])
n= (df['utm_n'].astype(str).str[:7])

#convert all values to KM
utm_east=[float(c)/1000 for c in e]
utm_north=[float(b)/1000 for b in n]

#Produce mean 
mean_utm_e= statistics.mean(utm_east)
mean_utm_n= statistics.mean(utm_north)

#produce error
x_error = [abs(mean_utm_e -f) for f in utm_east]
y_error = [abs(mean_utm_n- g) for g in utm_north]

#get combined error
error_km = [(((x_error[h])**2 +(y_error[h])**2))**(1/2) for h in range (len(x_error))]

error=[k*1000 for k in error_km]

#find standard deviation 
utme=np.array(utm_east)
utmn=np.array(utm_north)
std_e= np.std(utme)
std_n=np.std(utmn)

arr = np.vstack((utme,utmn))

std_1 = np.std(arr,axis=0)

#get the bounding box coordinates
goodderr_h=[mean_utm_e+std_e ,mean_utm_e+std_e,mean_utm_e-std_e,mean_utm_e-std_e,mean_utm_e+std_e]

goodderr_l=[mean_utm_n+std_n,mean_utm_n-std_n,mean_utm_n-std_n,mean_utm_n+std_n,mean_utm_n+std_n]

#plot 1st graph for utm_northing and utm_easting
fig, ax = plt.subplots()
ax.scatter(utm_east,utm_north,s=4)

#plot mean value calculated
plt.plot(mean_utm_e,mean_utm_n,'*',c= 'coral')

ax.ticklabel_format(useOffset=False)

#plot bounding box 
plt.plot(goodderr_h,goodderr_l,c='cyan')

plt.xticks(rotation=90)
plt.tight_layout()

#lable plot
plt.xlabel('UTM_EASTING(KM)')
plt.ylabel('UTM_NORTHING(KM)')
plt.title('gps_sensor_standing_data_in_UTM')

#add value for zone and letter in plot
plt.figtext(.6, .7,f"Zone   = {df.zone_a[1]}")
plt.figtext(.6, .75, f"Letter = {df.letter_a[1]}")

#plot second graph for error estimation
fig, bx=plt.subplots()
bx.scatter(df["time"],error,s=3,c='green')
bx.ticklabel_format(useOffset=False)
plt.xticks(rotation=90)
plt.tight_layout()

#lable plot
plt.xlabel('Time(s)')
plt.ylabel('Standard error(m)')
plt.title('gps_sensor_error_in_standing_data_in_UTM')

#show graphs
plt.show()
