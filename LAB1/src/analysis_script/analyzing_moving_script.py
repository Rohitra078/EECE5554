
import matplotlib.pyplot as plt
import pandas as pd
import numpy
from pylab import *

#Reading the csv file 

columns = ["header.stamp.secs","header.seq","utm_e", "utm_n","zone_a","letter_a"]
df = pd.read_csv("gps-walk_data.csv", usecols=columns)

#putting time 

stamp = df['header.seq']
df["time"] = [d-5 for d in stamp]

#calculate the line equation and remove excessive decimal points 
e= (df['utm_e'].astype(str).str[:6])
n= (df['utm_n'].astype(str).str[:7])

utm_east=[float(c)/1000 for c in e]
utm_north=[float(b)/1000 for b in n]

c=np.polyfit(utm_east, utm_north, 1)

#get the equation parameters
slope=float(c[0])
intercept=float(c[1])

#find eastimated utm_north value
utm_north_eastimated=[slope*float(r)+intercept for r in utm_east]

#find R_square value
corr_matrix = numpy.corrcoef(utm_north, utm_north_eastimated)
corr = corr_matrix[0,1]
R_sq = corr**2
R_sq=str(round(R_sq, 2))
print(f"R2 value is {R_sq}")

#find error with respect to the best straight line 
error = [(utm_north_eastimated[h]-utm_north[h])*1000 for h in range (len(utm_north_eastimated))]

#Plot the 1st graph with utm_easting and utm_northing
fig, ax = plt.subplots()
ax.scatter(utm_east,utm_north,s=3)
plt.plot(np.unique(utm_east), np.poly1d(c)(np.unique(utm_east)),c='green')
ax.ticklabel_format(useOffset=False)
plt.xticks(rotation=90)
plt.tight_layout()

#lable plot
plt.xlabel('UTM_EASTING(KM)')
plt.ylabel('UTM_NORTHING(KM)')
plt.title('gps_sensor_walking_data_in_UTM')

#Add description of letter and zone and r2
plt.figtext(.8, .65,f"Zone   = {df.zone_a[1]}")
plt.figtext(.8, .7, f"Letter = {df.letter_a[1]}")
plt.figtext(.8, .75,f'R2: {R_sq}')

#plot 2nd graph for error eastimation
fig, bx=plt.subplots()
bx.scatter(df["time"],error,s=3,c='coral')
bx.ticklabel_format(useOffset=False)
plt.xticks(rotation=90)
plt.tight_layout()

#lable plot
plt.xlabel('Time(s)')
plt.ylabel('standard error in m')
plt.title('gps_sensor_error_in_walking_data_in_UTM')

#show plot
plt.show()
